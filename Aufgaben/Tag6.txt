Tag 6 -------------------------------------------------------------

	-- Fortsetzung der Betrachtungen/Genomdaten-BLAST --

Willkommen zur zweiten Praktikumswoche! Hoffentlich habt Ihr alle einen guten Start in 
die zweite Haelfte, eine gute Vorstellung der Ziele und Ergebnisse die es in der 
zweiten Haelfte des Praktikums noch zu erreichen gilt und eine gut dokumentierte 
Zusammenfassung Eurer Ergebnisse aus der ersten Woche. Da die zu verwendenden Tools
nun bekannt und getestet sein sollten, wird die Aufgabenverteilung in der zweiten 
Woche etwas offener und eigenstaendiger erfolgen als in der ersten. Dennoch werden
mit den taeglichen Aufgaben weiterhin einige Anstoesse gegeben.

Aufgabe 1
Habt Ihr die von Euch entwickelte Datenpipeline bereits auf all Eure zugeteilten
Proteine angewendet? Wenn nicht, sollten diese Woche in jedem Fall auch die uebrigen 
Proteine untersucht werden. Welche Unterschiede in den Ergebnissen gibt es, bei 
welchen Proteinen ist es schwierig Ergebnisse zu erzielen und woran liegt das? Ist 
die Datenlage insb. bzgl der Proteindomains hinreichend fuer alle betrachteten
Proteine? Habt Ihr ALLE gefundenen Proteindomains gegen Eure Kandidatenproteine bzw
die mit BLAST gefundenen Homologen Proteine gesucht und sorgfaeltig Unterschiede bzgl 
zusaetzlichen oder fehlenden Domains dokumentiert? Koennt Ihr auf Basis des gegebenen
phylogenetischen Baumes Hypothesen zu Gewinn oder Verlust bestimmter Domains aufstellen?
Vergleicht unbedingt auch die mit Splitstree erstellten Baeume fuer alle 
Proteine und bringt diese in Zusammenhang. Faellt etwas auf? 

Noch einmal: Es ist ausgesprochen wichtig, sich bewusst zu machen, wie (a) ein normaler 
phylogenetischer Baum (wie der vorgegebene) und (b) ein mit Splitstree erstellter 
Baum zu lesen ist. Macht Euch noch einmal klar, dass durch die Inklusion einer
zusaetzlichen Dimension mehr Informationen in einem mit Splitstree enthaltenen Baum
konserviert ist. 
Weiterhin ist es fuer den Report und folgende Besprechungen sehr wichtig, dass 
alle Taxa im Baum entsprechend gelabeled sind. 

Eine weitere Moeglichkeit, zusaetzliche Daten zu Euren Zielproteinen zu sammeln soll
diese Woche noch durch die Betrachtung der Genomdaten einiger Spezies eroeffnet werden.
In der Datei Proteomtable.txt liegen auch Downloadlinks zu den 
Genomen einiger Spezies.
(Andernfall auch auf dem Praktikumsrechner, unter /scr/prak17/prakadmin, 
liegt ein Ordner mit heruntergeladenen Genomdaten). 

Es duerfte den meisten bekannt sein, dass mit blastall auch Proteinsequenzen gegen ein
Genom gesucht werden koennen. Dies kann hier genutzt werden, um die Informationslage
der in Woche 1 gefundenen Ergebnisse abzurunden. Dabei gibt es allerdings einige Dinge 
zu beachten:

-Wenn Ihr bereits wisst, dass es in Eurem Protein zu einem Verlust/Hinzukommen/Vertauschen
 von Proteindomains gekommen ist, wie wuerde das Eure Ergebnisse bei einer Suche im Genom
 beintraechtigen? Insbesondere wenn zwei Domains im Laufe der Evolution den Platz getauscht
 haben, kann dies Eure Treffer stark verfaelschen. Es ist deswegen sehr wichtig, sich
 den mutmasslichen Aufbau des Proteins fuer jede Spezies vorher bewusst zu machen, wobei 
 Ihr Euch auf die mit BLAST und HMMER gesammelten Daten stuetzen sollt. Dabei ist klar, 
 dass die Datenlage mit hoher Wahrscheinlichkeit unvollstaendig ist. Damit ist zu erwarten,
 dass es wahrscheinlich mit einer BLAST-Suche nicht getan ist, sondern das Zusammensetzen
 vieler Puzzlefragmente erforderlich ist.
-Um das "Puzzlespiel" zu verkomplizieren: Macht Euch noch einmal die Grundprinzipien des
 Splicing (Introns, Exons) bewusst. Was ist das Problem, wenn Euch die Position von 
 Introns im Gen nicht klar ist? Gibt es vielleicht Annotationen des Gens, in denen diese markiert sind?
 --> Wenn ja, waere es sicher sinnvoll eine modifizierte Version der Genomdatei ohne 
 diese zu erzeugen.
 Allerdings: Wie koennen Isoformen eines Proteins hier die Ergebnisse verfaelschen? 

Aufgabe 2 (-- optional)
Sucht Euch eines Eurer Proteine aus, und versucht mit BLAST (-tblastn) die Sequenz gegen
die zur Verfuegung gestellten Genomdaten zu suchen. Beachtet dabei unbedingt die 
obigen Informationen.

Aufgrund der vielen Variablen ist diese Aufgabe mit hoher Wahrscheinlichkeit schwieriger 
und zeitaufwaendiger als die uebrigen. Es wird daher explizit nicht erwartet dass diese
Analyse vollstaendig ist/fuer alle Proteine durchgefuehrt wird. Gruppen, die mit Ihren
uebrigen Untersuchungen bereits sehr weit sind, koennen sich hier an einer komplizierteren
Herausforderung versuchen und Ihre Ergebnisse in einen groesseren Kontext einordnen.


