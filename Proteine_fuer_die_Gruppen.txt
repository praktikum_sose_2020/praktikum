#Gruppe   Protein
#
Gruppe_1  PHC1
Gruppe_1  PHC2
Gruppe_1  PHC3
Gruppe_1  SCMH1
Gruppe_1  SCMH2
Gruppe_1  RING1A
Gruppe_1  RING1B
#
Gruppe_2  CBX2
Gruppe_2  CBX4
Gruppe_2  CBX6
Gruppe_2  CBX7
Gruppe_2  CBX8
Gruppe_2  RYBP
Gruppe_2  YAF2
#
Gruppe_3  ASXL1
Gruppe_3  ASXL2
Gruppe_3  MBD5
Gruppe_3  MBD6
Gruppe_3  KDM1B
Gruppe_3  KDM2B
#
Gruppe_4  PCL1
Gruppe_4  PCL2
Gruppe_4  PCL3
Gruppe_4  JARID2
Gruppe_4  AEBP2
Gruppe_4  EED
#
Gruppe_5  UTX
Gruppe_5  JMJD3
Gruppe_5  JMJD2
Gruppe_5  JMJD1
Gruppe_5  HCF1
Gruppe_5  PTIP
#
Gruppe_6  HDAC1
Gruppe_6  HDAC2
Gruppe_6  RBBP4
Gruppe_6  RBBP7
Gruppe_6  SAP30
Gruppe_6  SAP18
Gruppe_6  SIN3
#
Gruppe_7  MTA1
Gruppe_7  MTA2
Gruppe_7  MTA3
Gruppe_7  MBD3
Gruppe_7  MBD2
Gruppe_7  RBBP4
Gruppe_7  RBBP7
#
Gruppe_8  WHMT1
Gruppe_8  EHMT2
Gruppe_8  SUV39H1
Gruppe_8  SETD1B
Gruppe_8  KAT2A
Gruppe_8  p300
Gruppe_8  CBP
#
Gruppe_9  E2F6
Gruppe_9  L3MBTL2
Gruppe_9  MAX
Gruppe_9  MGA
Gruppe_9  DP-1
Gruppe_9  WDR5
Gruppe_9  DCAF
#
Gruppe_10 SET2
Gruppe_10 NSP1
Gruppe_10 SMYD2
Gruppe_10 FBXL10
Gruppe_10 SET8
Gruppe_10 SUV4/20
#
Gruppe_11 PCGF1
Gruppe_11 PCGF2
Gruppe_11 PCGF3
Gruppe_11 PCGF4
Gruppe_11 PCGF5
Gruppe_11 PCGF6
